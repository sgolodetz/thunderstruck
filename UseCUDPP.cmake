##################
# UseCUDPP.cmake #
##################

SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${thunderstruck_SOURCE_DIR}/cmake-modules/")
FIND_PACKAGE(CUDPP)
INCLUDE_DIRECTORIES(SYSTEM ${CUDPP_INCLUDE_DIR})
