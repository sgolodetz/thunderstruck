###################
# UseOpenCV.cmake #
###################

FIND_PACKAGE(OpenCV 2.4 REQUIRED)
INCLUDE_DIRECTORIES(SYSTEM ${OpenCV_INCLUDE_DIR})

